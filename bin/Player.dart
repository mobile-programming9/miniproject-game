import 'Board.dart';
import 'Obj.dart';

class Player extends Obj {
  /*late int x;
  late int y;
  late String symbol;*/
  late Board map;
  late int diamond = 0;

  /*Player(int x, int y, String symbol, Board map, int diamond)
      : this.symbol = symbol,
        this.map = map,
        this.diamond = diamond,
        super(x, y, "P");*/

  Player(super.x, super.y, super.symbol, this.map, this.diamond);

  bool walk(String direction) {
    switch (direction) {
      case 'N': // North เดินเหนือ
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      case 'S': // South เดินใต้
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'E': // East เดินตะวันออก
      case 'd':
        if (walkE()) {
          return false;
        }
        break;
      case 'W': // West เดินตะวันตก
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }
    checkDiamond();
    checkExit();
    return true;
  }

  void checkDiamond() {
    diamond += map.collDiamond(x, y);
    print("");
    print("Founded Diamond: ($diamond , 3)");
  }

  void checkExit() {
    if (map.isExit(x, y) && diamond == 3) {
      print(">>> You Win <<<");
      map.end = true;
    }
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y) && !map.isLava(x, y) && !map.isWall(x, y);
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y -= 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y += 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x += 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x -= 1;
    } else {
      return true;
    }
    return false;
  }

  @override
  toString() {
    return "Player{$map, $diamond}";
  }
}
