import 'dart:io';

import 'Board.dart';
import 'Diamond.dart';
import 'Player.dart';
import 'Lava.dart';
import 'Exit.dart';
import 'Wall.dart';

void main() {
  Board map = Board(10, 10);
  Player player = Player(1, 1, 'P', map, 0);
  Lava lava = Lava(3, 4);
  Exit exit = Exit(9, 9);
  map.setPlayer(player);
  map.setLava(lava);
  map.addObj(Lava(8, 2));
  map.addObj(Lava(2, 8));
  map.addObj(Lava(1, 6));
  map.addObj(Lava(3, 7));
  map.addObj(Lava(4, 2));
  map.addObj(Lava(6, 2));
  map.addObj(Lava(6, 6));
  map.addObj(Lava(7, 5));
  map.addObj(Lava(8, 8));
  map.addObj(Lava(9, 5));
  map.addObj(Wall(3, 6));
  map.addObj(Wall(5, 3));
  map.addObj(Wall(2, 3));
  map.addObj(Wall(4, 6));
  map.addObj(Wall(4, 8));
  map.addObj(Wall(5, 6));
  map.addObj(Wall(6, 1));
  map.addObj(Wall(6, 9));
  map.addObj(Wall(7, 7));
  map.addObj(Wall(8, 3));
  map.addObj(Wall(8, 7));
  map.addObj(Diamond(2, 9, 5));
  map.addObj(Diamond(5, 4, 5));
  map.addObj(Diamond(6, 7, 5));
  map.setLava(lava);
  map.setPlayer(player);
  map.setExit(exit);

  print("");
  print("𝚆𝙴𝙻𝙲𝙾𝙼𝙴  𝚃𝙾  𝙲𝙾𝙻𝙻𝙴𝙲𝚃 𝙳𝙸𝙰𝙼𝙾𝙽𝙳  𝙶𝙰𝙼𝙴");
  print("𝙿𝚕𝚎𝚊𝚜𝚎  𝙴𝚗𝚓𝚘𝚢  𝚝𝚘  𝙿𝙻𝙰𝚈");
  print("");
  List<String> str = [];
  str = [
    "--- RULE ---",
    "1.Player have to enter the input from keyboard to move.",
    "2.Player have to avoid obstructions. (Lava and Walls)",
    "3.the goal of the game is collect 3 Diamonds and go to the exit.",
    "4.Player can't exit untill collect all 3 diamonds."
  ];
  _runningDialog(str);

  while (true) {
    if (map.end == true) {
      map.showEnd();
      break;
    }
    map.showMap();
    String direction = stdin.readLineSync()!;

    if (direction == 'q') {
      break;
    }

    player.walk(direction);
  }
}

void _runningDialog(List<String> dialog, {speedText = 40, waitText = 1000}) {
  for (String element in dialog) {
    for (int i = 0; i < element.length; i++) {
      stdout.write(element[i]);
      sleep(Duration(milliseconds: speedText));
    }
    sleep(Duration(milliseconds: waitText));
    print("");
  }
}
