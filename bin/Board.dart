import 'dart:io';

import 'Diamond.dart';
import 'Exit.dart';
import 'Lava.dart';
import 'Obj.dart';
import 'Player.dart';
import 'Wall.dart';

class Board {
  late int width = 10;
  late int height = 10;
  late Player player;
  late Lava lava;
  var diamond = Diamond(2, 9, 5);
  late Exit exit;
  var objects = List<Obj>.filled(50, Obj(0, 0, ""), growable: true);
  bool end = false;

  // Obj[] objects = new Obj[20];
  int objCount = 0;

  Board(int width, int height) {
    this.width;
    this.height;
  }

  void addObj(Obj obj) {
    objects[objCount] = obj;
    objCount++;
  }

  void setPlayer(Player player) {
    this.player = player;
    addObj(player);
  }

  // void setDiamond(Diamond diamond) {
  //   this.diamond = diamond;
  //   addObj(diamond);
  // }

  void setLava(Lava lava) {
    this.lava = lava;
    addObj(lava);
  }

  void setExit(Exit exit) {
    this.exit = exit;
    addObj(exit);
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        symbol = objects[o].getSymbol();
      }
    }
    stdout.write('$symbol ');
  }

  bool inMap(int x, int y) {
    return (x >= 1 && x < width) && (y >= 1 && y < height);
  }

  bool isDiamond(int x, int y) {
    return diamond.isOn(x, y);
  }

  bool isExit(int x, int y) {
    return exit.isOn(x, y);
  }

  bool isLava(int x, int y) {
    return lava.isOn(x, y);
  }

  bool isWall(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Wall && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  // bool isEnd(int x, int y) {
  //   for (int o = 0; o < objCount; o++) {
  //     if (objects[o] is Exit && objects[o].isOn(x, y)) {
  //       end = true;
  //       return true;
  //     }
  //   }
  //   return false;
  // }

  int collDiamond(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Diamond && objects[o].isOn(x, y)) {
        // Obj diamond = objects[o];
        // objects[o].setSymbol("-");
        objects.remove(objects[o]);
        return 1; //diamond;
      }
    }
    return 0;
  }

  void showMap() {
    showTitle();
    print(player);
    print("");
    for (int y = 1; y < height; y++) {
      for (int x = 1; x < width; x++) {
        printSymbolOn(x, y);
      }
      showNewLine();
    }
  }

  void showTitle() {
    print("");
    print("Map");
    print("Walk(W, A, S, D)");
  }

  void showNewLine() {
    print("");
  }

  void showCell() {
    print("-");
  }

  void showObj(Obj obj) {
    print(obj.getSymbol());
  }

  void showPlayer() {
    print(player.getSymbol());
  }

  void showLava() {
    print(lava.getSymbol());
  }

  void showExit() {
    print(exit.getSymbol());
  }

  void showEnd() {
    print(" Congratulation!");
    print("");
  }
}
