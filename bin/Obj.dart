class Obj {
  int x;
  int y;
  String symbol;

  Obj(this.x, this.y, this.symbol);

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  void setSymbol(String string) {
    symbol = string;
  }
}
