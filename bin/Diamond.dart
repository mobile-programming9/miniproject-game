import 'dart:io';

import 'Obj.dart';

class Diamond extends Obj {
 int item;

  Diamond(int x, int y, int item)
      : this.item = item,
        super(x, y, "D");
}
